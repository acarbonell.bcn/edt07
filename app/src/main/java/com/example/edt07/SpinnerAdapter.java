package com.example.edt07;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;

public class SpinnerAdapter extends ArrayAdapter<Spinner1> {
    public SpinnerAdapter(@NonNull Context context, ArrayList<Spinner1> al) {
        super(context, 0, al);
    }

    public View initView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner, parent, false
            );


        }
        ImageView imgCover = convertView.findViewById(R.id.imgCover);
        TextView textCover = convertView.findViewById(R.id.textCover);

        Spinner1 currentItem = getItem(position);
        if (convertView != null){
            imgCover.setImageResource(currentItem.getImgCover());
            textCover.setText(currentItem.getTextCover());
        }
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
}
