package com.example.edt07;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity {
    private Button login;
    private TextView signup;
    private TextView fPsswd;
    private TextView email;
    private TextView pswwd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        //Hook
        login = findViewById(R.id.logIn);
        signup = findViewById(R.id.act2SignUp);
        fPsswd = findViewById(R.id.forgotPsswd);
        email = findViewById(R.id.textemailLi);
        pswwd= findViewById(R.id.psswodLi);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().toString().trim().equalsIgnoreCase("") ){
                    Toast.makeText(Activity2.this, "Email is null", Toast.LENGTH_SHORT).show();
                } if (pswwd.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(Activity2.this, "Password is null", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(Activity2.this, Wellcome.class);
                    startActivity(intent);
                }

            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity2.this, Signup.class);
                startActivity(intent);
            }
        });

        fPsswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity2.this, Forgotpassword.class);
                startActivity(intent);
            }
        });
    }

}