package com.example.edt07;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Forgotpassword extends AppCompatActivity {
    private Button rstpsswd;
    private TextView login;

    private LinearLayout lLayout3;
    ObjectAnimator obA1;
    ObjectAnimator obA2;
    ObjectAnimator obA3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        
        //Hook
        rstpsswd = findViewById(R.id.rstpsswd);
        login = findViewById(R.id.rstlogin);
        lLayout3 = findViewById(R.id.lLayout3);

        layoutStart(lLayout3);


        rstpsswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Forgotpassword.this, "Send email", Toast.LENGTH_SHORT).show();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Forgotpassword.this, Activity2.class);
                startActivity(intent);
            }
        });
    }
    public void layoutStart (View v){
        obA1 = ObjectAnimator.ofFloat(v, "scaleX", 0f, 1f);
        obA1.setDuration(1000);
        obA2 = ObjectAnimator.ofFloat(v, "scaleY", 0f, 1f);
        obA2.setDuration(1000);
        obA3 = ObjectAnimator.ofFloat(v, "alpha", 0f, 1f);
        obA3.setDuration(1400);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(obA1, obA2, obA3);
        set.start();
    }
}