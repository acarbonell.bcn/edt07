package com.example.edt07;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class Signup extends AppCompatActivity {
    private TextView  rtlogin;
    private Button signup;

    private EditText textDate;
    DatePickerDialog picker;

    private ArrayList<Spinner1> list;
    private SpinnerAdapter sAdapter;
    private Spinner sp;

    private Switch switch1;

    private LinearLayout lLayout2;
    ObjectAnimator obA1;
    ObjectAnimator obA2;
    ObjectAnimator obA3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Hook
        rtlogin = findViewById(R.id.rtlogin);
        signup = findViewById(R.id.signup);
        textDate = findViewById(R.id.textDate);
        sp = findViewById(R.id.spinner);
        switch1 = findViewById(R.id.switch1);
        lLayout2 = findViewById(R.id.lLayout2);

        layoutStart(lLayout2);


        rtlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Signup.this, Activity2.class);
                startActivity(intent);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Signup.this, Wellcome.class);
                startActivity(intent);
            }
        });

        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker = new DatePickerDialog(Signup.this,android.R.style.Theme_Holo_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        textDate.setText(dayOfMonth + "/" + month + "/" + year);
                    }
                }, year, month, day);
                picker.show();
            }
        });

        //Spinner
        list = new ArrayList<>();
        list.add(new Spinner1(R.drawable.bike01, "Bike 1"));
        list.add(new Spinner1(R.drawable.bike02, "Bike 2"));
        list.add(new Spinner1(R.drawable.bike03, "Bike 3"));
        list.add(new Spinner1(R.drawable.bike04, "Bike 4"));

        sAdapter = new SpinnerAdapter(this, list);
        sp.setAdapter(sAdapter);
        //lovgAdapter = new LovgAdapter2(this, lovgList);
        //SpinnerClass.setAdapter(lovgAdapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Spinner1 selectedItem = (Spinner1)  parent.getItemAtPosition(position);
                String lovgSelected = selectedItem.getTextCover();
                Toast.makeText(Signup.this, lovgSelected, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Toast.makeText(Signup.this, "Newsletter enable", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Signup.this, "Newsletter disable", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
    public void layoutStart (View v){
        obA1 = ObjectAnimator.ofFloat(v, "scaleX", 0f, 1f);
        obA1.setDuration(1000);
        obA2 = ObjectAnimator.ofFloat(v, "scaleY", 0f, 1f);
        obA2.setDuration(1000);
        obA3 = ObjectAnimator.ofFloat(v, "alpha", 0f, 1f);
        obA3.setDuration(1400);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(obA1, obA2, obA3);
        set.start();
    }
}