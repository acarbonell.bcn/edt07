package com.example.edt07;

import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

public class Spinner1 {
    private int imgCover;
    private String textCover;

    public Spinner1(int imgCover, String textCover) {
        this.imgCover = imgCover;
        this.textCover = textCover;
    }

    public int getImgCover() {
        return imgCover;
    }

    public void setImgCover(int imgCover) {
        this.imgCover = imgCover;
    }

    public String getTextCover() {
        return textCover;
    }

    public void setTextCover(String textCover) {
        this.textCover = textCover;
    }
}
